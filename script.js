const alphabetize = (a) => {
    return a.toLowerCase().split("").sort().join("").trim();
}

const getSetsOfFiveAnagrams = () => {
    const setsOfFive = []
    const anagrams = {}

    for (let palavra in palavras) {
        if(anagrams[alphabetize(palavras[palavra])] === undefined) {
            anagrams[alphabetize(palavras[palavra])] = [palavras[palavra]]
        } else {
            anagrams[alphabetize(palavras[palavra])].push(palavras[palavra])
        }
    }

    for (let anagram in anagrams) {
        if (anagrams[anagram].length >= 5) {
            setsOfFive.push(anagrams[anagram])
            let setString = document.createElement('p')
            setString.classList.add('result')
            setString.innerHTML = anagrams[anagram].join(' ')
            document.getElementById('content').appendChild(setString)
        }
    }
    return setsOfFive
}
getSetsOfFiveAnagrams()